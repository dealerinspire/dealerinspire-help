window.Vue = require('vue');

Vue.config.productionTip = false;

import ExampleComponent from "./components/ExampleComponent";

Vue.component('example', ExampleComponent);

new Vue({}).$mount("#help-app");
