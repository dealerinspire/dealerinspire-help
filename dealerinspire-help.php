<?php

declare(strict_types = 1);

/*
Plugin Name: Dealer Inspire - Help
Plugin URI: http://www.dealerinspire.com/
Description: Help
Version: 1.0.0
Author: Dealer Inspire - Features Team
Author URI: http://www.dealerinspire.com
*/

namespace DealerInspire\Help;

(function (): void {
    require_once __DIR__ . '/bootstrap/plugin.php';
})();
