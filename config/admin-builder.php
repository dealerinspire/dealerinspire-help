<?php

declare(strict_types = 1);

return [
    'paths' => [
        __DIR__ . '/settings.json',
    ],
];
