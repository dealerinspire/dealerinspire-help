<?php

declare(strict_types = 1);

use DealerInspire\Help\Providers;

return [
    /*
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     █ Plugin Environment
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░ This value determines the environment the plugin is running. This may
     ░ determine how you prefer to configure various services the plugin
     ░ utilizes.
     */
    'env' => env('APP_ENV', 'production'),

    /*
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     █ Autoloaded Service Providers
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░ The service providers listed here will be called automatically on load
     ░ of the Wordpress application. Each service provider has it's register
     ░ then boot methods called.
     */
    'providers' => [
        Providers\AdminServiceProvider::class,
    ],
];
