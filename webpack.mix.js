const mix = require('laravel-mix');
const cssImport = require('postcss-import')
const cssNesting = require('postcss-nesting')
const rootPath = Mix.paths.root.bind(Mix.paths)
const tailwindcss = require('tailwindcss')

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

mix
  .setPublicPath(path.normalize('dist'))

  .js('resources/js/app', 'dist')

  .postCss('resources/css/app.css', 'dist', [
    cssImport(),
    cssNesting(),
    tailwindcss('tailwind.config.js')
  ])

  .purgeCss({
    content: [
      rootPath('src/**/*.php'),
      rootPath('resources/**/*.html'),
      rootPath('resources/**/*.js'),
      rootPath('resources/**/*.php'),
      rootPath('resources/**/*.vue'),
    ],
    defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || []
  });
