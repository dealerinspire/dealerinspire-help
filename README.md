# Dealer Inspire - Help 💀

THIS IS A WORK IN PROGRESS REPOSITORY. The following goals are being kept in
mind during development:

- Should be easy for any developer to pick up and learn how to get going
- Compiled assets _should not_ be committed
- Composer vendor directories _should not_ be committed
- Environment variables (aka secrets) _should not_ be committed