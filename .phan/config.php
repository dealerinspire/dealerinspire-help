<?php

use Phan\Config;

/**
 * This configuration will be read and overlaid on top of the
 * default configuration. Command line arguments will be applied
 * after this file is read.
 *
 * @see src/Phan/Config.php
 * See Config for all configurable options.
 *
 * A Note About Paths
 * ==================
 *
 * Files referenced from this file should be defined as
 *
 * ```
 *   Config::projectPath('relative_path/to/file')
 * ```
 *
 * where the relative path is relative to the root of the
 * project which is defined as either the working directory
 * of the phan executable or a path passed in via the CLI
 * '-d' flag.
 */
return [
    // CONFIGURING FILES
    'analyzed_file_extensions' => ['php'],
    'consistent_hashing_file_order' => false,
    'directory_list' => [
         'src',
         'dependencies/vendor',
    ],
    'exclude_analysis_directory_list' => [
        'dependencies/vendor',
    ],
    'exclude_file_list' => [],
    'exclude_file_regex' => '',
    'file_list' => [],
    'include_analysis_file_list' => [],

    // ISSUE FILTERING
    'disable_file_based_suppression' => false,
    'disable_line_based_suppression' => false,
    'disable_suppression' => false,
    'minimum_severity' => 0,
    'suppress_issue_types' => [
        'PhanTypePossiblyInvalidDimOffset',
        'PhanTypeInvalidDimOffset',
        'PhanTypeArraySuspiciousNullable',
        'PhanPluginUnknownPropertyType',
    ],
    'whitelist_issue_types' => [],

    // ANALYSIS
    'allow_missing_properties' => false,
    'analyze_signature_compatibility' => true,
    'assume_no_external_class_overrides' => false,
    'autoload_internal_extension_signatures' => [],
    'backward_compatibility_checks' => false,
    'cache_polyfill_asts' => false,
    'check_docblock_signature_param_type_match' => false,
    'check_docblock_signature_return_type_match' => true,
    'enable_class_alias_support' => false,
    'enable_extended_internal_return_type_plugins' => true,
    'enable_include_path_checks' => true,
    'enable_internal_return_type_plugins' => true,
    'exception_classes_with_optional_throws_phpdoc' => [],
    'generic_types_enabled' => true,
    'globals_type_map' => [],
    'guess_unknown_parameter_type_using_default' => true,
    'ignore_undeclared_functions_with_known_signatures' => false,
    'ignore_undeclared_variables_in_global_scope' => false,
    'include_paths' => ['.'],
    'inherit_phpdoc_types' => true,
    'max_literal_string_type_length' => 200,
    'maximum_recursion_depth' => 3,
    'parent_constructor_required' => [],
    'phpdoc_type_mapping' => [],
    'plugin_config' => [],
    // https://github.com/phan/phan/tree/master/.phan/plugins
    'plugins' => [
        '.phan/plugins/AlwaysReturnPlugin.php',
        '.phan/plugins/DuplicateArrayKeyPlugin.php',
        '.phan/plugins/DuplicateExpressionPlugin.php',
        '.phan/plugins/PrintfCheckerPlugin.php',
        '.phan/plugins/UnknownElementTypePlugin.php'
    ],
    'prefer_narrowed_phpdoc_param_type' => false,
    'prefer_narrowed_phpdoc_return_type' => true,
    'processes' => 1,
    'quick_mode' => false,
    'read_magic_method_annotations' => true,
    'read_magic_property_annotations' => true,
    'read_type_annotations' => true,
    'runkit_superglobals' => [],
    'simplify_ast' => false,
    'warn_about_relative_include_statement' => false,
    'warn_about_undocumented_throw_statements' => true,

    // ANALYSIS OF A PHP VERSION
    'allow_method_param_type_widening' => false,
    'polyfill_parse_all_element_doc_comments' => true,
    'pretend_newer_core_methods_exist' => true,
    'target_php_version' => '7.1',

    // TYPE CASTING
    'array_casts_as_null' => false,
    'null_casts_as_any_type' => true,
    'null_casts_as_array' => false,
    'scalar_array_key_cast' => false,
    'scalar_implicit_cast' => false,
    'scalar_implicit_partial' => [],
    // The strict_*_checking can introduce false positives. Not interested.
    'strict_method_checking' => false,
    'strict_param_checking' => false,
    'strict_property_checking' => false,
    'strict_return_checking' => false,

    // DEAD CODE DETECTION
    'constant_variable_detection' => false,
    'dead_code_detection' => false,
    'dead_code_detection_prefer_false_negative' => true,
    'force_tracking_references' => true,
    'unused_variable_detection' => false,
    'unused_variable_detection_assume_override_exists' => false,
    'warn_about_redundant_use_namespaced_class' => true,

    // OUTPUT
    'color_issue_messages_if_supported' => true,
    'color_scheme' => [],
    'disable_suggestions' => false,
    'skip_missing_tokenizer_warning' => false,
    'skip_slow_php_options_warning' => true,
    'suggestion_check_limit' => 1000
];
