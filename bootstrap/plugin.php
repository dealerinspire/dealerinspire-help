<?php

declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../dependencies/vendor/autoload.php';

$plugin = new DealerInspire\Help\Foundation\Plugin(
    new DI\Container()
);

$plugin->bootstrap();
$plugin->registerProviders();
$plugin->bootProviders();

return $plugin;
