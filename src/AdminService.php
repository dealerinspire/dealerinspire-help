<?php

declare(strict_types = 1);

namespace DealerInspire\Help;

use DealerInspire\Help\Foundation\WordpressProxy;

class AdminService
{
    const ADMIN_PAGE_SLUG = 'dealerinspire-help';

    private $wordpress;

    public function __construct(WordpressProxy $wordpress)
    {
        $this->wordpress = $wordpress;
    }

    public function addAssets(): void
    {
        if (! $this->onAdminSettingsPage()) {
            return;
        }

        $this->wordpress->addStyle(
            'plugin-base-admin',
            $this->wordpress->pluginsUrl('dist/app.css'),
            ['di-admin-builder']
        );

        $this->wordpress->addScript(
            'plugin-base-admin',
            $this->wordpress->pluginsUrl('dist/app.js'),
            [],
            true
        );
    }

    private function onAdminSettingsPage(): bool
    {
        return self::ADMIN_PAGE_SLUG === ($_GET['page'] ?? '');
    }
}
