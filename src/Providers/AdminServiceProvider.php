<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Providers;

use DealerInspire\Help\AdminService;
use DealerInspire\Help\Foundation\Providers\ServiceProvider;
use DealerInspire\Help\Foundation\WordpressProxy;

class AdminServiceProvider extends ServiceProvider
{
    const ADMIN_BUILDER_ACTION = 'admin_builder_wordpress_settings_configs';

    /*
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     █ Register Container Bindings (required)
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░ Use the register method to register bindings to the container. These
     ░ bindings are lazy loaded only when they are actually needed.
     */
    public function register(): void
    {
        $this->container->set(AdminService::class, function (): AdminService {
            return new AdminService(
                $this->container->make(WordpressProxy::class)
            );
        });
    }

    /*
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     █ Add Wordpress Actions or Filters (optional)
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░ Use the boot method on a service provider to add Wordpress Actions and
     ░ filters, bind event listeners, set up routes or create a view composer.
     ░ The boot method is called on each service provider after all providers
     ░ have been registered.
     */
    public function boot(): void
    {
        $wordpress = $this->container->make(WordpressProxy::class);

        // TODO: Maybe this should be a default thing where if a
        // `admin-builder.json` file exists in config this is called?
        $wordpress->addAction(
            'admin_builder_wordpress_settings_configs',
            function (): array {
                return $this->container->make('config')
                    ->get('admin-builder.paths') ?? [];
            }
        );

        $wordpress->addAction(
            'admin_enqueue_scripts',
            [$this->container->make(AdminService::class), 'addAssets']
        );
    }
}
