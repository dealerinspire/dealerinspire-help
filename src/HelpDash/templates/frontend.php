<style>
	ul#starting-group {
	display: block;
}
ul#building-a-page-group {
	display: none;
}
ul#publishing-a-blog-post-group {
	display: none;
}
ul#changing-my-hours-group {
	display: none;
}
ul#using-special-offers-group {
	display: none;
}
</style>

<h3>What do you need help with?</h3>
<ul id="starting-group">
	<li><a id="start-building-a-page" href="#">Building a Page</a></li>
	<li><a id="start-publishing-a-blog-post" href="#">Publishing a Blog Post</a></li>
	<li><a id="start-changing-my-hours" href="#">Changing My Hours</a></li>
	<li><a id="start-using-special-offers" href="#">Using Special Offers</a></li>
</ul>
<ul id="building-a-page-group">
	<li><a href="#">BUILDING A PAGE ITEM 1</a></li>
	<li><a href="#">BUILDING A PAGE ITEM 2</a></li>
	<li><a href="#">BUILDING A PAGE ITEM 3</a></li>
	<li><a href="#">BUILDING A PAGE ITEM 4</a></li>
	<li><a id="return-building-a-page" href="#">Return to Start</a></li>
</ul>
<ul id="publishing-a-blog-post-group">
	<li><a href="#">PUBLISHING A BLOG POST ITEM 1</a></li>
	<li><a href="#">PUBLISHING A BLOG POST ITEM 2</a></li>
	<li><a href="#">PUBLISHING A BLOG POST ITEM 3</a></li>
	<li><a href="#">PUBLISHING A BLOG POST ITEM 4</a></li>
	<li><a id="return-publishing-a-blog-post" href="#">Return to Start</a></li>
</ul>
<ul id="changing-my-hours-group">
	<li><a href="#">CHANGING MY HOURS ITEM 1</a></li>
	<li><a href="#">CHANGING MY HOURS ITEM 2</a></li>
	<li><a href="#">CHANGING MY HOURS ITEM 3</a></li>
	<li><a href="#">CHANGING MY HOURS ITEM 4</a></li>
	<li><a id="return-changing-my-hours" href="#">Return to Start</a></li>
</ul>
<ul id="using-special-offers-group">
	<li><a href="#">USING SPECIAL OFFERS ITEM 1</a></li>
	<li><a href="#">USING SPECIAL OFFERS ITEM 2</a></li>
	<li><a href="#">USING SPECIAL OFFERS ITEM 3</a></li>
	<li><a href="#">USING SPECIAL OFFERS ITEM 4</a></li>
	<li><a id="return-using-special-offers" href="#">Return to Start</a></li>
</ul>

<script>
// expand buttons
const expandBuildingAPage=document.getElementById("start-building-a-page");
const expandPublishingABlogPost=document.getElementById("start-publishing-a-blog-post");
const expandChangingMyHours=document.getElementById("start-changing-my-hours");
const expandUsingSpecialOffers=document.getElementById("start-using-special-offers");
// collapse buttons
const collapseBuildingAPage=document.getElementById("return-building-a-page");
const collapsePublishingABlogPost=document.getElementById("return-publishing-a-blog-post");
const collapseChangingMyHours=document.getElementById("return-changing-my-hours");
const collapseUsingSpecialOffers=document.getElementById("return-using-special-offers");
// expand targets
const targetStartingGroup=document.getElementById("starting-group");
const targetBuildingAPage=document.getElementById("building-a-page-group");
const targetPublishingABlogPost=document.getElementById("publishing-a-blog-post-group")
const targetChangingMyHours=document.getElementById("changing-my-hours-group")
const targetUsingSpecialOffers=document.getElementById("using-special-offers-group")

// expand actions
expandBuildingAPage.onclick = function expandBuildingAPageAction() {
	targetStartingGroup.style.display = "none";

	targetBuildingAPage.style.display = "block";
}
expandPublishingABlogPost.onclick = function expandPublishingABlogPostAction() {
	targetStartingGroup.style.display = "none";

	targetPublishingABlogPost.style.display = "block";
}
expandChangingMyHours.onclick = function expandChangingMyHoursAction() {
	targetStartingGroup.style.display = "none";

	targetChangingMyHours.style.display = "block";
}
expandUsingSpecialOffers.onclick = function expandUsingSpecialOffersAction() {
	targetStartingGroup.style.display = "none";

	targetUsingSpecialOffers.style.display = "block";
}
// collapse actions
collapseBuildingAPage.onclick = function collapseBuildingAPageAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapsePublishingABlogPost.onclick = function collapsePublishingABlogPostAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapseChangingMyHours.onclick = function collapseChangingMyHoursAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapseUsingSpecialOffers.onclick = function collapseUsingSpecialOffersAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
</script>