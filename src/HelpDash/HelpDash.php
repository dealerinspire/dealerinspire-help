<?php

namespace DealerInspire\Dashboards\HelpDash;

use DealerInspire\Dashboards\Dashboards;

/**
 * Controls the HelpDash Dashboard. Still not working. Need to setup namespace for admin builder. Widget currently in admin builder
 */
class HelpDash extends Dashboards
{
    /**
     * Defines if the HelpDash Dashboard should be shown.
     *
     * @var bool
     */
    protected $showDashboard;
    /**
     * Reference to the plugin path.
     *
     * @var string
     */
    private $pluginPath;

    /**
     * Reference to the plugin url.
     *
     * @var string
     */
    private $pluginUrl;

    /**
     * Accepts json config to assemble the AdminPage from.
     */
    public function __construct()
    {
        $this->pluginUrl = \plugins_url('/dealerinspire-admin-builder/');
        // @phan-suppress-next-line PhanUndeclaredConstant
        $this->pluginPath = WP_PLUGIN_DIR . '/dealerinspire-admin-builder/';
        $this->showDashboard = true;
    }

    /**
     * Runs the Dashboard setup.
     */
    public function registerDashboard(): void
    {
            $this->registerHooks();
    }

    /**
     * Runs the Dashboard setup.
     */
    public function dashboardHandler(): void
    {
        echo $this->buildWidget();
    }

    /**
     * Builds the Wrapper HTML and calls other methods to build Widget.
     *
     * @return string
     */
    public function buildWidget(): string
    {
        $widgetWrapper = $this->getTemplate('frontend.php');
        try {
            ob_start();
            include $widgetWrapper;
        } catch (\Throwable $e) {
            ob_get_clean();
            throw $e;
        }

        return ob_get_clean();
    }

    /**
     * Enqueues assets for HelpDash.
     */
    public function initializeWidget(): void
    {

        $version = filemtime($this->pluginPath . 'resources/css/admin-builder.css');

        \wp_enqueue_script('di-admin-builder', $this->pluginURL . 'src/Dashboards/HelpDash/js/helpdash-dashboard.js', $version, true);
        \wp_enqueue_style('di-admin-builder', $this->pluginUrl . 'resources/css/admin-builder.css', ['wp-codemirror'], $version);
        \wp_enqueue_style('di-admin-builder', $this->pluginUrl . 'src/Dashboards/HelpDash/css/style.css', $version);

        \wp_add_dashboard_widget(
            'di_dashboard_helpdash',
            'DI Help',
            [$this, 'dashboardHandler']
        );

        add_screen_option('layout_columns', ['default' => 3]);
    }

    /**
     * Registers WP hooks for the HelpDash Dashboard.
     */
    private function registerHooks(): void
    {
        \add_action('wp_dashboard_setup', [$this, 'initializeWidget']);
    }

    /**
     * Returns template file.
     *
     * @return string
     */
    private function getTemplate(string $template): string
    {
        return dirname(__FILE__) . '/templates/' . $template;
    }
}


