// expand buttons
const expandBuildingAPage=document.getElementById("start-building-a-page");
const expandPublishingABlogPost=document.getElementById("start-publishing-a-blog-post");
const expandChangingMyHours=document.getElementById("start-changing-my-hours");
const expandUsingSpecialOffers=document.getElementById("start-using-special-offers");
// collapse buttons
const collapseBuildingAPage=document.getElementById("return-building-a-page");
const collapsePublishingABlogPost=document.getElementById("return-publishing-a-blog-post");
const collapseChangingMyHours=document.getElementById("return-changing-my-hours");
const collapseUsingSpecialOffers=document.getElementById("return-using-special-offers");
// expand targets
const targetStartingGroup=document.getElementById("starting-group");
const targetBuildingAPage=document.getElementById("building-a-page-group");
const targetPublishingABlogPost=document.getElementById("publishing-a-blog-post-group")
const targetChangingMyHours=document.getElementById("changing-my-hours-group")
const targetUsingSpecialOffers=document.getElementById("using-special-offers-group")

// expand actions
expandBuildingAPage.onclick = function expandBuildingAPageAction() {
	targetStartingGroup.style.display = "none";

	targetBuildingAPage.style.display = "block";
}
expandPublishingABlogPost.onclick = function expandPublishingABlogPostAction() {
	targetStartingGroup.style.display = "none";

	targetPublishingABlogPost.style.display = "block";
}
expandChangingMyHours.onclick = function expandChangingMyHoursAction() {
	targetStartingGroup.style.display = "none";

	targetChangingMyHours.style.display = "block";
}
expandUsingSpecialOffers.onclick = function expandUsingSpecialOffersAction() {
	targetStartingGroup.style.display = "none";

	targetUsingSpecialOffers.style.display = "block";
}
// collapse actions
collapseBuildingAPage.onclick = function collapseBuildingAPageAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapsePublishingABlogPost.onclick = function collapsePublishingABlogPostAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapseChangingMyHours.onclick = function collapseChangingMyHoursAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}
collapseUsingSpecialOffers.onclick = function collapseUsingSpecialOffersAction() {
	targetBuildingAPage.style.display = "none";
	targetPublishingABlogPost.style.display = "none";
	targetChangingMyHours.style.display = "none";
	targetUsingSpecialOffers.style.display = "none";

	targetStartingGroup.style.display = "block";
}