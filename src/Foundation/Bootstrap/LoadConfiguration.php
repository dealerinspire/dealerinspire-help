<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation\Bootstrap;

use DealerInspire\Help\Foundation\ConfigRepository;
use DI\Container;
use SplFileInfo;
use Symfony\Component\Finder\Finder;

class LoadConfiguration
{
    public function bootstrap(Container $container): void
    {
        $container->set('config', function (): ConfigRepository {
            $repository = new ConfigRepository();

            $this->load($repository);

            return $repository;
        });
    }

    private function load(ConfigRepository $config): void
    {
        foreach ($this->getConfigFiles() as $key => $path) {
            $config->set($key, require $path);
        }
    }

    private function getConfigFiles(): array
    {
        $files = [];

        $config_path = realpath(__DIR__ . '/../../../config/');

        $config_files = Finder::create()
            ->files()
            ->name('*.php')
            ->in($config_path);

        foreach ($config_files as $file) {
            $directory = $this->getNestedDirectory($file, $config_path);

            $files[$directory . basename($file->getRealPath(), '.php')] = $file->getRealPath();
        }

        ksort($files, SORT_NATURAL);

        return $files;
    }

    private function getNestedDirectory(
        SplFileInfo $file,
        string $config_path
    ): string {
        $directory = $file->getPath();

        if ($nested = trim(str_replace($config_path, '', $directory), DIRECTORY_SEPARATOR)) {
            $nested = str_replace(DIRECTORY_SEPARATOR, '.', $nested);
        }

        return $nested;
    }
}
