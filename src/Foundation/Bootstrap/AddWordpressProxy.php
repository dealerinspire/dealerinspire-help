<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation\Bootstrap;

use DealerInspire\Help\Foundation\WordpressProxy;
use DI\Container;

class AddWordpressProxy
{
    public function bootstrap(Container $container): void
    {
        $container->set(WordpressProxy::class, function (): WordpressProxy {
            return new WordpressProxy();
        });
    }
}
