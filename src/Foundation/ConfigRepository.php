<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation;

use ArrayAccess;
use DealerInspire\Help\Foundation\Support\Arr;

class ConfigRepository implements ArrayAccess
{
    protected $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    /**
     * @suppress PhanPluginUnknownMethodReturnType
     */
    public function get(string $key, $default = null)
    {
        if (is_array($key)) {
            return $this->getMany($key);
        }

        return Arr::get($this->items, $key, $default);
    }

    /**
     * @suppress PhanPluginUnknownMethodReturnType, PhanPluginUnknownMethodParamType
     */
    public function getMany($keys)
    {
        $config = [];

        foreach ($keys as $key => $default) {
            if (is_numeric($key)) {
                [$key, $default] = [$default, null];
            }

            $config[$key] = Arr::get($this->items, $key, $default);
        }

        return $config;
    }

    /**
     * @suppress PhanPluginUnknownMethodParamType
     */
    public function has($key): bool
    {
        return Arr::has($this->items, $key);
    }

    /**
     * @suppress PhanPluginUnknownMethodParamType
     */
    public function set($key, $value = null): void
    {
        $keys = is_array($key) ? $key : [$key => $value];

        foreach ($keys as $key => $value) {
            Arr::set($this->items, $key, $value);
        }
    }

    public function offsetExists($key)
    {
        return $this->has($key);
    }

    public function offsetGet($key)
    {
        return $this->get($key);
    }

    public function offsetSet($key, $value): void
    {
        $this->set($key, $value);
    }

    public function offsetUnset($key): void
    {
        $this->set($key, null);
    }
}
