<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation;

class WordpressProxy
{
    /**
     * @suppress PhanUndeclaredFunction
     */
    public function addAction(string $tag, callable $callable, int $priority = 10, int $accepted_args = 1): void
    {
        \add_action($tag, $callable, $priority, $accepted_args);
    }

    /**
     * @suppress PhanUndeclaredFunction
     */
    public function addFilter(string $tag, callable $callable, int $priority = 10, int $accepted_args = 1): void
    {
        \add_filter($tag, $callable, $priority, $accepted_args);
    }

    /**
     * @suppress PhanUndeclaredFunction
     */
    public function addScript(string $handle, string $src, array $deps = [], bool $in_footer = false): void
    {
        \wp_enqueue_script($handle, $src, $deps, null, $in_footer);
    }

    /**
     * @suppress PhanUndeclaredFunction
     */
    public function addStyle(string $handle, string $source, array $deps = []): void
    {
        \wp_enqueue_style($handle, $source, $deps);
    }

    /**
     * @suppress PhanUndeclaredFunction
     */
    public function pluginsUrl(string $path): string
    {
        return \plugins_url(
            rtrim('/dealerinspire-help/' . $path, '/')
        );
    }
}
