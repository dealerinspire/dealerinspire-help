<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation\Providers;

use DI\Container;

abstract class ServiceProvider
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function register(): void
    {
    }
}
