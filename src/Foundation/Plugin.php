<?php

declare(strict_types = 1);

namespace DealerInspire\Help\Foundation;

use DealerInspire\Help\Foundation\Bootstrap\AddWordpressProxy;
use DealerInspire\Help\Foundation\Bootstrap\LoadConfiguration;
use DI\Container;

class Plugin
{
    protected $bootstrappers = [
        AddWordpressProxy::class,
        LoadConfiguration::class,
    ];

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function bootstrap(): void
    {
        foreach ($this->bootstrappers as $bootstrapper) {
            $this->container->make($bootstrapper)
                ->bootstrap($this->container);
        }
    }

    public function registerProviders(): void
    {
        $providers = $this->container->make('config')->get('plugin.providers');

        foreach ($providers as $provider) {
            $provider = new $provider($this->container);
            $provider->register();
        }
    }

    public function bootProviders(): void
    {
        $providers = $this->container->make('config')->get('plugin.providers');

        foreach ($providers as $provider) {
            $provider = $this->container->make($provider);
            if (method_exists($provider, 'boot')) {
                $provider->boot();
            }
        }
    }
}
