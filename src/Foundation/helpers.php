<?php

declare(strict_types = 1);

if (! function_exists('env')) {
    /**
     * @suppress PhanPluginUnknownFunctionReturnType, PhanPluginUnknownFunctionParamType
     */
    function env(string $key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (($valueLength = strlen($value)) > 1 && $value[0] === '"' && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * @suppress PhanPluginUnknownFunctionReturnType, PhanPluginUnknownFunctionParamType
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}
